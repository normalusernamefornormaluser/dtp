<!DOCTYPE html>
<html lang="ru">
  
  <head>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <meta charset="utf-8">
    <title>Контакты</title>
  </head>
  <body>
  <?php include 'header.php'; ?>

    <main>
<div class="px-4 py-5 my-5 text-center">
    <h1 class="display-5 fw-bold">Связь с разработчиком проекта</h1>
    <div class="col-lg-6 mx-auto">
      <p class="lead mb-4">Разработчик проекта - студент Московского Политехнического Университета Шибаев Семён Сергеевич, связаться можно по адресу электронной почты: normalusernamefornormaluser@gmail.com. А также можно</p>
      <div class="d-grid gap-1 d-sm-flex justify-content-sm-center">
        <a href="https://web.telegram.org/k/#@alivefrustration" class="btn btn-primary btn-lg px-4 gap-3">Связаться в Telegram</a>
      </div>
    </div>
  </div>
</section>
    </main>
    <?php include 'footer.php'; ?>
  </body>
</html>