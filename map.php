<!DOCTYPE html>
<html lang="ru">
  
  <head>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <meta charset="utf-8">
    <title>Интерактивная карта ДТП</title>
  </head>
  <?php include 'connect.php'; ?>
  <script src="https://api-maps.yandex.ru/2.1/?apikey=3f6e7749-ca99-4d44-9d76-c6358e5ff87f&lang=ru_RU" type="text/javascript">
    </script> 
    <script src="https://yandex.st/jquery/2.2.3/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
   
    ymaps.ready(init);
    function init(){
        
        var myMap = new ymaps.Map("map", {
            center: [56.129065, 40.406625],
            zoom: 15
        });
       
    var myGeoObjects = [];
    var i = 0;
<?php
    $sql = mysqli_query($mysql, 'SELECT latitude, longitude, address, category, datetime, severity FROM `mytable`');
while ($result = mysqli_fetch_array($sql)) { ?>
    myGeoObjects[i] = new ymaps.GeoObject({
            
            geometry: {
                type: "Point",
                coordinates: [<?php echo $result['latitude'] ?>, <?php echo $result['longitude'] ?>]
            },
            
            properties: {
                hintContent: '<?php echo $result['category']; ?>',
                balloonContent:  'Категория:  <?php echo $result['category']; ?> ; Дата и Время:  <?php echo $result['datetime']; ?> ; Степень тяжести: <?php echo $result['severity'];?>'
            }
        })
       i++;
    <?php }?>
var myClasterer = new ymaps.Clusterer();
myClasterer.add(myGeoObjects);
myMap.geoObjects.add(myClasterer);
}
</script>
<body>
<?php include 'header.php'; ?>
<div class='container-fluid justify-content-center'>
    <div class = 'text-center'>
<h1 class="display-5 fw-bold">Интерактивная карта ДТП</h1>

<p class="text-center text-muted">На данной карте вы можете просмотреть информацию о ДТП с 01.01.2015 по 31.12.2021. (нажмите на отметку на карте чтобы узнать подробнее)</p>
</div>
    <div id="map" style="width: 1000px; height: 1000px; display: block;
    margin-left: auto;
    margin-right: auto;"></div>
    </div>
    <?php include 'footer.php'; ?>
</body>
</html>