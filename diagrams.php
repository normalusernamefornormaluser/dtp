<!DOCTYPE html>
<html lang="ru">
  
  <head>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <meta charset="utf-8">
    <?php include 'connect.php'; ?>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <title>Диаграммы</title>
  </head>
 
  <script type="text/javascript">
    
    google.charts.load('current', {'packages':['corechart']});


google.charts.setOnLoadCallback(drawChart);


function drawChart() {


  var data = new google.visualization.DataTable();
  data.addColumn('string', 'Район');
  data.addColumn('number', 'ДТП');
  data.addRows([
     <?php $sql = mysqli_query($mysql, 'SELECT region, COUNT(*) AS a FROM `mytable` WHERE 1 GROUP BY region ORDER BY a DESC;');
     $result = mysqli_fetch_array($sql); ?>
      [ '<?php echo $result['region'] ?>', <?php echo $result['a'] ?>]
<?php  while ($result = mysqli_fetch_array($sql)) { ?>
    ,
    [ '<?php echo $result['region'] ?>', <?php echo $result['a'] ?>]
<?php } ?>
  ]);


  var options = {'title':'Количество дтп по районам',
                 'width':600,
                 'height':450};

    
var data1 = new google.visualization.DataTable();
  data1.addColumn('string', 'Степень тяжести');
  data1.addColumn('number', 'Количество');
  data1.addRows([
     <?php $sql = mysqli_query($mysql, 'SELECT severity, COUNT(*) as a FROM `mytable` WHERE 1 GROUP BY severity ORDER BY a DESC;');
     $result = mysqli_fetch_array($sql); ?>
      [ '<?php echo $result['severity'] ?>', <?php echo $result['a'] ?>]
<?php  while ($result = mysqli_fetch_array($sql)) { ?>
    ,
    [ '<?php echo $result['severity'] ?>', <?php echo $result['a'] ?>]
<?php } ?>
  ]);
  var options1 = {'title':'Количество дтп по степени тяжести',
                 'width':600,
                 'height':450};

                 var data2 = new google.visualization.DataTable();
  data2.addColumn('string', 'Категория');
  data2.addColumn('number', 'Количество');
  data2.addRows([
     <?php $sql = mysqli_query($mysql, 'SELECT category, COUNT(*) AS a FROM `mytable` WHERE 1 GROUP BY category ORDER by a DESC LIMIT 10;');
     $result = mysqli_fetch_array($sql); ?>
      [ '<?php echo $result['category'] ?>', <?php echo $result['a'] ?>]
<?php  while ($result = mysqli_fetch_array($sql)) { ?>
    ,
    [ '<?php echo $result['category'] ?>', <?php echo $result['a'] ?>]
<?php } ?>
  ]);

  var options2 = {'title':'Количество дтп по категории',
                 'width':600,
                 'height':450};

 
  var chart1 = new google.visualization.PieChart(document.getElementById('pieChart1'));
  chart1.draw(data, options);
  var chart2 = new google.visualization.PieChart(document.getElementById('pieChart2'));
  chart2.draw(data1, options1);
  var chart3 = new google.visualization.PieChart(document.getElementById('pieChart3'));
  chart3.draw(data2, options2);
}
  </script>
  <body>
<?php include 'header.php'; ?>
<div class = 'text-center'>
    <h1 class="display-5 fw-bold">Диаграммы со статистикой</h1>
    </div>
    <div class='container-fluid d-flex justify-content-center'>
    <div id="pieChart1"></div>
    <div id="pieChart2"></div>
    <div id="pieChart3"></div>
    </div>

    <?php include 'footer.php'; ?>
</body>
</html>