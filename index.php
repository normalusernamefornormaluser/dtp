<!DOCTYPE html>
<html lang="ru">
  
  <head>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <meta charset="utf-8">
    <title>Главная страница</title>
  </head>
  <body>
  <?php include 'header.php'; ?>

    <main>
<div class="px-4 py-5 my-5 text-center">
    <h1 class="display-5 fw-bold">База данных ДТП Владимирской области на основе открытых данных</h1>
    <div class="col-lg-6 mx-auto">
      <p class="lead mb-4">Это приложение создано для демонстрации обстановки на дороге на территории Владимирской области, открытые данные взяты с сайта <a href = "https://www.kaggle.com/datasets/shlykov/russia-road-accidents">kaggle</a>. В данном приложении вы можете ознакомиться с:</p>
      <div class="d-grid gap-2 d-sm-flex justify-content-sm-center">
        <a href="map.php" class="btn btn-primary btn-lg px-4 gap-3">Интерактивной картой</a>
        <a href="diagrams.php" class="btn btn-outline-secondary btn-lg px-4">Статистикой в диаграммах</a>
      </div>
    </div>
  </div>
</section>
    </main>
    <?php include 'footer.php'; ?>
  </body>
</html>