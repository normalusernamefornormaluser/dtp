<div class="container">
  <footer id="contact" class="py-3 my-4">
    <ul class="nav justify-content-center border-bottom pb-3 mb-3">
      <li class="nav-item"><a href="index.php" class="nav-link px-2 text-muted">На главную</a></li>
      <li class="nav-item"><a href="https://web.telegram.org/k/#@alivefrustration" class="nav-link px-2 text-muted">Оставить обратную связь в Telegram</a></li>
    </ul>
    <p class="text-center text-muted">Курсовая работа. Открытые данные взяты с <a href = "https://www.kaggle.com/datasets/shlykov/russia-road-accidents">kaggle</a>. E-mail для связи - normalusernamefornormaluser@gmail.com</p>
  </footer>
</div>