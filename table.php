<!DOCTYPE html>
<html lang="ru">
  
  <head>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <meta charset="utf-8">
    <title>Последние ДТП</title>
    <?php include 'connect.php'; ?>
  </head>
  <body>
  <?php include 'header.php'; ?>

    <main>

    <div class="px-4 py-5 my-5 text-center">
    <h1 class="display-5 fw-bold">10 последних ДТП</h1>

    <table class="table table-striped">
        <tr>
            <th scope="col">
                Широта
</th>
<th scope="col">
    Долгота
</th>
<th scope="col">
    Район
</th>
<th scope="col">
    Адрес
</th>
<th scope="col">
    Категория
</th>
<th scope="col">
    Степень тяжести
</th>
<th scope="col">
    Дата и время
</th>
</tr>
    <?php
$sql = mysqli_query($mysql, 'SELECT * FROM `mytable` WHERE 1 ORDER BY datetime DESC LIMIT 10;');
while ($result = mysqli_fetch_array($sql)) {
?>
<tr> 
<td><?php echo $result['latitude'];?></td>
<td><?php echo $result['longitude'];?></td> 
<td><?php echo $result['region'];?></td> 
<td><?php echo $result['address'];?></td> 
<td><?php echo $result['category'];?></td>
<td><?php echo $result['severity'];?></td>
<td><?php echo $result['datetime'];?></td>
</tr>
<?php
}
?>
</table>
</div>
    </main>
    <?php include 'footer.php'; ?>
  </body>
</html>